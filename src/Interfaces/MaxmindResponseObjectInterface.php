<?php

declare( strict_types = 1 );

namespace DTNL\MaxmindClient\Interfaces;

use GuzzleHttp\Psr7\Response;

interface MaxmindResponseObjectInterface {

    /**
     * Create an Object from the API responses
     *
     * @param \SimpleXMLElement $response
     */
    public function __construct( \SimpleXMLElement $response );

    /**
     * Return an array representation of the data
     *
     * @return array
     */
    public function toArray() : array;

    /**
     * Return a string representation of the data
     *
     * @return string
     */
    public function __toString() : string;

}
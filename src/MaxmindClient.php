<?php

declare( strict_types = 1 );

namespace DTNL\MaxmindClient;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use DTNL\MaxmindClient\Exceptions\HttpResponseException;

class MaxmindClient {

    /**
     * @var string[]
     */
    const VALID_REQUEST_TYPES = [
        'list', 'availability', 'rates',
        'calendar', 'supplements', 'list_couponcode',
        'list_corporate_rate', 'confirmation',
    ];

    /**
     * @var string[]
     */
    const VALID_LANGUAGES = [
        'nl', 'en', 'de',
        'fr', 'es', 'ru'
    ];

    /**
     * @var string[]
     */
    const VALID_CURRENCIES = [
        'EUR', 'GBP', 'USD',
        'COP', 'RUB', 'AUD',
        'CAD', 'SGD','UAH'
    ];

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var integer
     */
    protected $hotel_id;

    /**
     * @var string|null
     */
    private $username;

    /**
     * @var string|null
     */
    private $password;

    /**
     * Language
     * 
     * Possible values:
     * 'nl', 'en', 'de', 'fr', 'es' and 'ru'
     * @var string|null
     */
    protected $language;

    /**
     * @var string|null
     */
    protected $currency;

    /**
     * Constructor
     *
     * @param Client $client
     */
    public function __construct( Client $client ) {
        $this->client = $client;
    }

    public function requestRates( array $request_data ) : Response {
        return $this->request( 'rates', $request_data );
    }

    /**
     * Perform a generic request to the API
     *
     * @param string $request_type
     *   Request type. Possible values:
     *   list, availability, rates, calendar, supplements,
     *   list_couponcode, list_corporate_rate, confirmation
     * @param array $request_data
     * @return Response
     */
    public function request( string $request_type, array $request_data ) : Response {

        if ( !in_array( $request_type, self::VALID_REQUEST_TYPES ) ) {
            throw new Exceptions\InvalidRequestTypeException(
                'Invalid request type "' . $request_type . '".'
            );
        }
          
        $request_data['rq'] = $request_type;
        $query['json'] = json_encode( $request_data );
        
        if ( isset( $this->language ) ) {
            $query['hl'] = $this->language;
        }

        if ( isset( $this->currency ) ) {
            $query['au'] = $this->currency;
        }

        $params['query'] = $query;

        // Add credentials if given
        if ( isset( $this->username ) && isset( $this->password ) ) {
            $params['auth'] = [
                $this->username,
                $this->password
            ];
        }

        /**
         * @var Response $response
         */
        $response = $this->client->get( '', $params );

        // Check the https status
        if ( $response->getStatusCode() !== 200 ) {
            throw new Exceptions\HttpResponseException( $response->getStatusCode() );
        }

        // Check if Maxmind is complaining
        $content = (string) $response->getBody();
        if ( strpos( $content, '{' ) === 0
          || strpos( $content, '<' . $request_type . '_rq>{' ) === 0 
        ) {
            $content = str_replace( '<' . $request_type . '_rq>', '', $content );
            $error = json_decode( $content );
            if ( isset( $error->error ) ) {
                throw new Exceptions\MaxmindApiErrorException( $error->error );
            } else {
                throw new Exceptions\MaxmindApiErrorException( 'Something went terribly wrong.' );
            }
        }

        // All go, return response
        return $response;
    }

    /**
     * Perform a generic request to the Api,
     * return result as XML
     *
     * @param string $request_type
     * @param array $request_data
     * @return void
     */
    public function requestXml( string $request_type, array $request_data ) : \SimpleXMLElement {
        $response = $this->request( $request_type, $request_data );
        $xml_string = (string) $response->getBody();
        return simplexml_load_string( $xml_string );
    }

    /**
     * @return integer
     */ 
    public function getHotelId() {
        return $this->hotel_id;
    }

    /**
     * @param integer $hotel_id
     * @return self
     */ 
    public function setHotelId( int $hotel_id ) : self {
        $this->hotel_id = $hotel_id;
        return $this;
    }

    /**
     * @param string $username
     * @param string $password
     * @return void
     */
    public function setCredentials( string $username, string $password ) : self {
        $this->username = $username;
        $this->password = $password;
        return $this;
    }

    /**
     * Remove credentials
     *
     * @return self
     */
    public function unsetCredentials() : self {
        $this->username = null;
        $this->password = null;
        return $this;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return self
     */
    public function setLanguage( string $language ) : self {
        if ( !in_array( $language, self::VALID_LANGUAGES ) ) {
            throw new Exceptions\InvalidLanguageException(
                'Invalid language: "' . $language . '".'
            );
        }
        $this->language = $language;
        return $this;
    }

    /**
     * Return language
     *
     * @return string
     */
    public function getLanguage() : string {
        return $this->lanuage;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return self
     */
    public function setCurrency( string $currency ) : self {
        if ( !in_array( $currency, self::VALID_CURRENCIES ) ) {
            throw new Exceptions\InvalidCurrencyException(
                'Invalid currency: "' . $currency . '".'
            );
        }
        $this->currency = $currency;
        return $this;
    }

    /**
     * Return currency
     *
     * @return string
     */
    public function getCurrency() : string {
        return $this->currency;
    }

}
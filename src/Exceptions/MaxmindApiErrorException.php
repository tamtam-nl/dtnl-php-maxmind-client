<?php

declare( strict_types = 1 );

namespace DTNL\MaxmindClient\Exceptions;

class MaxmindApiErrorException extends MaxmindClientException {};
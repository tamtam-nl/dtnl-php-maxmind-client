<?php

declare( strict_types = 1 );

namespace DTNL\MaxmindClient\Exceptions;

class HttpResponseException extends MaxmindClientException {
    
    protected $http_status_code;

    /**
     * @param integer $http_status_code
     */
    public function __construct( int $http_status_code ) {
        $this->http_status_code = $http_status_code;
        $this->message = 'Invalid Http Response Code: ' . $http_status_code;
    }

    /**
     * Set the HTTP status code
     *
     * @param integer $http_status_code
     * @return void
     */
    protected function setStatusCode( int $http_status_code ) {
        $this->http_status_code = $http_status_code;
        return $this;
    }

    /**
     * Get the HTTP status code
     *
     * @return integer
     */
    public function getStatusCode() : int {
        return $this->http_status_code;
    }
};
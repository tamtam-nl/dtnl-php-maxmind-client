<?php

declare( strict_types = 1 );

namespace DTNL\MaxmindClient\Exceptions;

class InvalidLanguageException extends MaxmindClientException {};
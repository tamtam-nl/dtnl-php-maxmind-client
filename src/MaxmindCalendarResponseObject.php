<?php

declare( strict_types = 1 );

namespace DTNL\MaxmindClient;

class MaxmindCalendarResponseObject
    implements Interfaces\MaxmindResponseObjectInterface {

    /**
     * @var \SimpleXMLElement
     */
    protected $xml_object;

    /**
     * @var array|null
     */
    protected $array = null;

    /**
     * {@inheritDoc}
     */
    public function __construct( \SimpleXMLElement $response ) {
        $this->xml_object = $response;
    }

    /**
     * {@inheritDoc}
     */
    public function toArray() : array {

        if ( is_array( $this->array ) ) { return $this->array; }

        $result = [];
        foreach ( $this->xml_object as $date ) {

            $data = [
                'arrival' => new \DateTime( (string) $date->attributes()->arrival ),
                'departure' => new \DateTime( (string) $date->attributes()->departure ),
            ];

            $rooms = [];
            foreach ( $date->room as $room ) {
                $rooms[] = [
                    'id' => (int) $room->attributes()->id,
                    'available' => (int) $room->available,
                    'rate' => isset( $room->rate ) ? (int) $room->rate : -1,
                ];
            }

            $data['rooms'] =  $rooms;
            $result[] = $data;

        }

        $this->array = $result;

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function __toString() : string {

        $string = self::class . ' [' . PHP_EOL;

        foreach ( $this->toArray() as $date ) {
    
            $title = '  '
                . $date['arrival']->format( 'Y-m-d' )
                . ' - ' . $date['arrival']->format( 'Y-m-d' )
                 . PHP_EOL;
            $string .= $title;
            $string .= '  ' . str_repeat( '-', strlen( $title ) - 3 );
            $string .= PHP_EOL;

            $string .= '  ';
            $string .= sprintf(
                '%-5s %-10s %-5s',
                'Id', 'Available', 'Rate'
            );
            $string .= PHP_EOL;

            foreach ( $date['rooms'] as $room ) {
                $string .= '  ';
                $string .= sprintf(
                    '%-5s %-10s %-5s',
                    $room[ 'id' ],
                    $room['available'],
                    $room['rate']
                );
                $string .= PHP_EOL;
            }

            $string .= PHP_EOL;
        }
        $string .= ']' . PHP_EOL;
        return $string;
    }

}
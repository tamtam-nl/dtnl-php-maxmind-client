<?php

declare( strict_types = 1 );

namespace DTNL\MaxmindClient;

class MaxmindAvailabilityResponseObject
    implements Interfaces\MaxmindResponseObjectInterface {

    /**
     * @var \SimpleXMLElement
     */
    protected $xml_object;

    /**
     * @var array|null
     */
    protected $array = null;

    /**
     * {@inheritDoc}
     */
    public function __construct( \SimpleXMLElement $response ) {
        $this->xml_object = $response;
    }

    /**
     * {@inheritDoc}
     */
    public function toArray() : array {

        if ( is_array( $this->array ) ) { return $this->array; }

        $result = [];
        foreach ( $this->xml_object->hotel->package as $package ) {

            $package_id = (int) $package->attributes()->id;
            $data = [
                'id' => $package_id,
                'name' => (string) $package->attributes()->name
            ];

            $rooms = [];
            foreach ( $package->room as $room ) {
                $room_id = (int) $room->attributes()->id;
                $rooms[ $room_id ] = [
                    'id' => $room_id,
                    'name' => (string) $room->attributes()->name,
                    'persons' => (int) $room->attributes()->persons,
                    'available' => (int) $room->available,
                    // Ignoring extra_beds and extra_beds_only_for_children
                    'rate' => (float) $room->rate,
                ];
            }

            $data['rooms'] = $rooms;
            $result[ $package_id ] = $data;

        }

        $this->array = $result;

        return $result;
    }

        /**
     * {@inheritDoc}
     */
    public function __toString() : string {
        return print_r( $this->toArray(), TRUE );
    }

}
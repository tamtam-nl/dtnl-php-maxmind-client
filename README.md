Maxmind API Client
==================

Basic Maxmind / Maxengine API Client library.

Usage
-----

```php

use DTNL\MaxmindClient\MaxmindClient;
use DTNL\MaxmindClient\MaxmindCalendarResponseObject;

// Set up HTTP Client
$client_config = ;
$client = new GuzzleHttp\Client(
	[ 'base_uri' => 'https://secure.maxengine.eu/api/xml_v1/', ]
);

// Set up Maxmind Client
$mmc = new MaxmindClient( $client );
$mmc
    ->setHotelId( $hotel_id )
    ->setLanguage( $lang_code )
    ->setCurrency( 'EUR' )
	->setCredentials( $username, $password );

// Call API,use "calendar" call
$response = $mmc->requestXml( 'calendar', [
        'hotel_id' => $hotel_id,
        'package_id' => $package_id,
        'room_id' => $room_id,
        'month' => $yyyy_mm,
        'nights' => 1,
        'adults' => 1,
    ]
);

// Create Object from Response
$result = new MaxmindCalendarResponseObject( $response );
```
